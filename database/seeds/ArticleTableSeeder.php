<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Article;

class ArticleTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i < 10; $i++){
            Article::create([
                'title' => str_random(10),
                'content' => str_random(255)
            ]);
        }
    }
}