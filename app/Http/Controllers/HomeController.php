<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use App\Auther;

class HomeController extends Controller
{
    public function index(){
        $category = Category::all();
        $categoryFilter = Category::pluck('name','id');
        $autherFilter = Auther::pluck('name','id');
        $article = Article::all();
        return view('home', compact('article', 'category', 'categoryFilter', 'autherFilter'));
    }
}
