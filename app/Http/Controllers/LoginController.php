<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Article;
use App\Category;
use App\Auther;

class LoginController extends Controller
{
    //登入
    public function login(LoginRequest $request){

        $loginData  = $request->session()->get('userdata');
        $loginData  = $request->all();
        $username   = $loginData['username'];
        $password   = $loginData['password'];

        $results    = Auther::where('username', '=' ,$username)->where('password', '=', $password)->get(['id', 'name', 'username']);

        if(count($results) >= 1){
            $auther_id  = $results['0']['id'];
            $name       = $results['0']['name'];
            $username   = $results['0']['username'];

            $request->session()->put('auther_id',$auther_id);
            $request->session()->put('name',$name);
            $request->session()->put('username',$username);

            return redirect()->route('article.index');
        }else{
            return redirect()->route('auther.index')->with([
                'message'=>'帳號或密碼錯誤',
            ]);
        }
    }

    //登出
    public function logout(Request $request){

        $request->session()->flush();
        return redirect()->route('home');
    }

}
