<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use Illuminate\Http\Request;
use App\Article;
use App\Auther;
use App\Category;


class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->session()->has('auther_id')){
            $categoryFilter = Category::pluck('name','id');
            $sessionID = $request->session()->get('auther_id');
            $article = Article::where('auther_id', '=', $sessionID)->get();
            return view('article.index', compact('article', 'categoryFilter'));
        }else{
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *w
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('article.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $articleData = $request->all();
        $auther_id = $request->session()->get('auther_id');
        $results = Article::create([
            'auther_id' => $auther_id,
            'category_id' => $articleData['category_id'],
            'title' => $articleData['title'],
            'content' => $articleData['content'],
        ]);
        return redirect()->route('article.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categoryFilter = Category::pluck('name','id');
        $autherFilter = Auther::pluck('name','id');
        $article = Article::find($id);
        return view('article.viewarticle', compact('article', 'categoryFilter', 'autherFilter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        $category = Category::all();
        $categoryFilter = Category::pluck('name','id');
        // var_dump($article);die;
        return view('article.edit', compact('article', 'category', 'categoryFilter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, $id)
    {

        $articleData = $request->all();
        $title = $articleData['title'];
        $category_id = $articleData['category_id'];
        $content = $articleData['content'];
        $results = Article::where('id', '=', $id)->update([
            'title' => $title,
            'category_id' => $category_id,
            'content' => $content,
        ]);
        return redirect()->route('article.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Article::destroy($id);
        return redirect()->route('article.index');
    }
}
