<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'content' => 'required',
            //
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute 為必填。'
        ];
    }

    public function attributes()
    {
        return [
            'title' => '標題',
            'content' => '文章內容',
        ];
    }

}
