<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AutherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required | unique:authers',
            'username' => 'required | unique:authers',
            'password' => 'required',
            'check_password' => 'same:password',
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute 為必填。',
            'same' => ':attribute 與密碼不一致',
            'unique' => ':attribute 已被使用',
        ];
    }

    public function attributes()
    {
        return [
            'name' => '暱稱',
            'username' => '帳號',
            'password' => '密碼',
            'check_password' => '第二次密碼'

        ];
    }

    protected function formatErrors(Validator $validator)
    {
        $errorMessages = '';
        //這邊為一個錯誤訊息最後配一個<br/>
        foreach ($validator->messages()->all(':message<br>') as $message) {
            $errorMessages .= $message;
        }
        return $errorMessages;
    }

}
