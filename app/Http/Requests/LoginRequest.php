<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required',
            'password' => 'required',
            //
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute 為必填。'
        ];
    }

    public function attributes()
    {
        return [
            'username' => '帳號',
            'password' => '密碼',
        ];
    }

    protected function formatErrors(Validator $validator)
    {
        $errorMessages = '';
        //這邊為一個錯誤訊息最後配一個<br/>
        foreach ($validator->messages()->all(':message<br>') as $message) {
            $errorMessages .= $message;
        }
        return $errorMessages;
    }
}
