<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auther extends Model
{
    //
    protected $table = 'authers';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'username', 'password'];

    public function article()
    {
        return $this->hasMany('App\Article', 'foreign_key');
    }
}
