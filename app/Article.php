<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $table = 'articles';

    protected $primaryKey = 'id';

    protected $foreignKey = 'auther_id';

    protected $fillable = ['auther_id', 'category_id', 'title', 'content'];

    public function auther()
    {
        return $this->belongsTo('App\Auther');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
