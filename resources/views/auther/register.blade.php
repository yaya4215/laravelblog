@extends('app')
@section('content')
<form action="{{url('auther')}}" method="POST">
@CSRF
    <table>
        <tr>
            <td>請輸入暱稱：</td>
            <td><input type="text" name="name"></td>
        </tr>
        <tr>
            <td>請輸入帳號：</td>
            <td><input type="text" name="username"></td>
        </tr>
        <tr>
            <td>請輸入密碼：</td>
            <td><input type="password" name="password"></td>
        </tr>
        <tr>
            <td>確認密碼：</td>
            <td><input type="password" name="check_password"></td>
        </tr>
        <tr style=" text-align: right;">
            <td colspan="2">
                <input type="submit" value="送出"">
                <a href="{{url('/')}}">返回</a>
            </td>
        </tr>
        @if(Session::get('message'))
        <tr style=" text-align: right; color: red;">
            <td colspan="2">{{ Session::get('message') }}</td>
        </tr>
        @endif
    </table>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <p>
                {{ $error }}
            </p>
        @endforeach
    </div>
    @endif
</form>
@stop
