@extends('app')
@section('content')
<form action="{{url('/login')}}" method="POST">
@CSRF
    <table>
        <tr>
            <td>帳號：</td>
            <td><input type="text" name="username"></td>
        </tr>
        <tr>
            <td>密碼：</td>
            <td><input type="password" name="password"></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="登入">
                <a href="{{url('auther/create')}}">註冊</a>
                <a href="{{url('/')}}">返回</a>
            </td>
        </tr>
        @if(Session::get('message'))
        <tr>
            <td colspan="2">{{ Session::get('message') }}</td>
        </tr>
        @endif
    </table>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <p>
                {{ $error }}
            </p>
            @endforeach
        </div>
    @endif
</form>
@stop
