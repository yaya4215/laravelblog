@extends('app')
@section('content')
<table border="1" width="200px;">
    <tr>
        <td>標題：</td>
        <td>{{$article->title}}</td>
    </tr>
    <tr>
        <td>分類：</td>
        <td> {{ $categoryFilter[$article->category_id] }} </td>
    </tr>
    <tr>
        <td>作者：</td>
        <td> {{ $autherFilter[$article->auther_id] }} </td>
    </tr>
    <tr>
        <td>內容：</td>
        <td>{{$article->content}}</td>
    </tr>
</table>
<a href="{{url('/')}}">返回</a>
@stop
