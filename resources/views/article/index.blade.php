@extends('app')
@section('content')
    <a href="{{ url('article/create') }}">新增</a>
    <a href="{{ url('/') }}">返回</a>
    <table border="1">
    <tr>
        <td>ID</td>
        <td>分類</td>
        <td>標題</td>
        <td>預覽內容</td>
        <td>發布時間</td>
        <td>最後編輯</td>
        <td colspan="2">功能</td>
    </tr>
        @foreach ($article as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $categoryFilter[$item->category_id] }}</td>
                <td><a href="{{ url('/article/'.$item->id) }}">{{ $item->title }}</td>
                <td>{{ mb_substr( $item->content, 0, 20, "utf-8") }}</td>
                <td>{{ $item->created_at }}</td>
                <td>{{ $item->updated_at }}</td>
                <td><a href="{{ url('article/'.$item->id.'/edit') }}">編輯</a></td>
                <td>
                    <form action="{{ url('article/'.$item->id) }}" method="POST">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" value="刪除">
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    @if($article == [])
    <div>目前尚無貼文</div>
    @endif
@stop
