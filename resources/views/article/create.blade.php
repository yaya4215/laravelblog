@extends('app')
@section('content')
<form action="{{url('article')}}" method="POST">
@CSRF
    <table style="text-align: left;">
        <tr>
            <td>標題：</td>
            <td><input type="text" name="title"></td>
        </tr>
        <tr>
            <td>分類：</td>
            <td>
                <select name="category_id">
                    @foreach ($category as $item)
                    <option value ="{{$item->id}}">{{$item->name}}</option>
                    @endforeach
                </select>
            </td>
        </tr>
        <tr>
            <td>內容：</td>
            <td><textarea name="content" cols="30" rows="10"></textarea></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type="submit" value="送出文章">
                <a role="btn" href="{{url('article')}}">返回</a>
            </td>
        </tr>
    </table>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <p>
                {{ $error }}
            </p>
            @endforeach
        </div>
    @endif
</form>

@stop
