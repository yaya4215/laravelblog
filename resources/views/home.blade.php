@extends('app')
@section('content')
<div>
    {{-- 選擇分類：
    <select onchange="filter()">
        <option value ="0">全部</option>
        @foreach ($category as $item)
        <option value ="{{$item->id}}">{{$item->name}}</option>
        @endforeach
    </select> --}}

    @if(Session::has('auther_id'))
    <span>
        <a href="{{ url('article') }}">{{ Session::get('name') }}的文章</a> | <a href="{{ url('/logout') }}">登出</a>
    </span>
    @else
    <span>
        <a href="{{ url('auther') }}">會員登入</a> | <a href="{{ url('auther/create') }}">會員註冊</a>
    </span>
    @endif
</div>
<div>
    <table border="1">
    <tr>
        <td>ID</td>
        <td>分類</td>
        <td>標題</td>
        <td>預覽內容</td>
        <td>作者</td>
        <td>發布時間</td>
        <td>最後編輯</td>
    </tr>
    @foreach ($article as $item)
    <tr>
        <td>{{ $item->id }}</td>
        <td>{{ $categoryFilter[$item->category_id] }}</td>
        <td><a href="{{ url('/article/'.$item->id) }}">{{ $item->title }}</a></td>
        <td>{{ mb_substr( $item->content, 0, 20, "utf-8") }}</td>
        <td>{{ $autherFilter[$item->auther_id] }}</td>
        <td>{{ $item->created_at }}</td>
        <td>{{ $item->updated_at }}</td>
    </tr>
    @endforeach
    </table>
</div>
@stop
